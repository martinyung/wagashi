class Product < ApplicationRecord
  has_many :discounts
  has_many :product_purchases
  has_many :purchases, through: :product_purchases

  validates :name, presence: true
  validates :product_type, presence: true, uniqueness: true
  validates :price, presence: true, numericality: { greater_than_or_equal_to: 0 }
end



