class Customer < ApplicationRecord
  has_many :discounts
  validates :name, presence: true
end
