class CreatePurchases < ActiveRecord::Migration[5.0]
  def change
    create_table :purchases do |t|
      t.integer :customer_id, null: false
      t.decimal :total, :precision => 8, :scale => 2
      t.timestamps
    end
  end
end
