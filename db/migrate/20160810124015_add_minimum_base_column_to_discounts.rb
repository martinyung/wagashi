class AddMinimumBaseColumnToDiscounts < ActiveRecord::Migration[5.0]
  def change
    add_column :discounts, :minimum_purchase, :integer, default: 0
  end
end
