require 'rails_helper'

RSpec.describe Discount, type: :model do
  it { is_expected.to belong_to(:product) }
  it { is_expected.to belong_to(:customer) }
  it { is_expected.to have_many(:products_purchases) }
  it { is_expected.to validate_presence_of(:product_id) }
  it { is_expected.to validate_presence_of(:customer_id) }
  it { is_expected.to validate_presence_of(:discount_type) }
  it { is_expected.to validate_presence_of(:minimum_purchase) }
  it { is_expected.to validate_presence_of(:rate) }
  it { is_expected.to validate_presence_of(:status) }
  it { is_expected.to validate_numericality_of(:minimum_purchase).is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_numericality_of(:rate).is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_numericality_of(:status).is_greater_than_or_equal_to(0) }
end

