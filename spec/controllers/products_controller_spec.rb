require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  let (:product_params) { {name: "vala", product_type: "Noma", price: 50.12} }
  let (:product) { Product.create(product_params)}

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    before(:each) { get :show, id: product.id }
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "has the correct assignment" do
      expect(assigns(:product)).not_to be_nil
      expect(assigns(:product).name).to eq product.name
    end
  end

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #edit" do
    it "returns http success" do
      get :edit, id: product.id
      expect(response).to have_http_status(:success)
    end
  end

end
